from django.urls import path
from .views import *

urlpatterns = [
    path('', homepage_view, name='home'),
    path('guestbook/', guestbook_view, name='guestbook'),
    path('visualdesign/', visualdesign_view, name='visualdesign'),
    path('videography/', videography_view, name='videography'),
    path('personalSchedule/', personalSchedule, name="personalSchedule"),
    path('personalSchedule/delete/', delete_all_events, name="delete_all_events")
]

