from django import forms
from .models import Activities
from django.forms import ModelForm




class EventForm(forms.ModelForm):
	class Meta: #menuju ke models
		model = Activities
		fields = ['name','date','place','category']