from django.shortcuts import render
from django.shortcuts import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Activities
from .forms import EventForm

def homepage_view(request):
    return render(request, "myfirstwebsite.html")


def guestbook_view(request):
    return render(request, "guestbook.html")


def visualdesign_view(request):
    return render(request, "visualdesign.html")


def videography_view(request):
    return render(request, "videography.html")

def personalSchedule(request):
	events = Activities.objects.all().values()

	if request.method == "POST":
		form = EventForm(request.POST)
		if form.is_valid():
			new_event = form.save()
			return HttpResponseRedirect(reverse('personalSchedule'))
	else :
			form = EventForm()

	response = {'form':form, 'events':events}
	return render(request, "activities.html", {'form':form, 'events':events})

def delete_all_events(request):
	events = Activities.objects.all().delete()
#	form = EventForm

	return HttpResponseRedirect(reverse('personalSchedule'))